package discountservice

class UrlMappings {

    static mappings = {
        get "/discounts.json"(action:"list", controller:"discount")

        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
