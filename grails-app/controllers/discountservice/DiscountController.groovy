package discountservice

import grails.converters.JSON
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class DiscountController {

    DiscountService discountService

    static responseFormats = ['json']

    Integer limit = 10

    def list() {
        if (params.points != null)
            render Discount.findAllByPointsLessThan(params.points, [max:limit]) as JSON
        else render Discount.list(max: limit) as JSON
    }
}
