package discountservice

class Discount {

    String play
    Integer discount
    Integer points

    static constraints = {
        play nullable : false, blank: false
        discount nullable : false, min: 0
        points nullable : false, min: 0
    }
}
