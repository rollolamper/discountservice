package discountservice

import grails.gorm.services.Service

@Service(Discount)
interface DiscountService {

    List<Discount> list(Map args)

}