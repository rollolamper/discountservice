package discountservice

class BootStrap {

    def init = { servletContext ->

        new Discount(play: "Shakespeare in Love", discount: 30, points: 20).save()
        new Discount(play: "Shakespeare in Love", discount: 40, points: 30).save()
        new Discount(play: "Hamlet", discount: 10, points: 5).save()
        new Discount(play: "Hamlet", discount: 15, points: 10).save()
        new Discount(play: "Hamlet", discount: 20, points: 12).save()
        new Discount(play: "Hamilton", discount: 5, points: 30).save()
        new Discount(play: "Hamilton", discount: 10, points: 50).save()
        new Discount(play: "Hamilton", discount: 20, points: 80).save()
        new Discount(play: "CATS", discount: 55, points: 40).save()
        new Discount(play: "CATS", discount: 60, points: 50).save()
    }
    def destroy = {
    }
}
