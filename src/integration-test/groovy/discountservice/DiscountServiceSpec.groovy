package discountservice

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class DiscountServiceSpec extends Specification {

    DiscountService discountService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new Discount(...).save(flush: true, failOnError: true)
        //new Discount(...).save(flush: true, failOnError: true)
        //Discount discount = new Discount(...).save(flush: true, failOnError: true)
        //new Discount(...).save(flush: true, failOnError: true)
        //new Discount(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //discount.id
    }

    void "test get"() {
        setupData()

        expect:
        discountService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<Discount> discountList = discountService.list(max: 2, offset: 2)

        then:
        discountList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        discountService.count() == 5
    }

    void "test delete"() {
        Long discountId = setupData()

        expect:
        discountService.count() == 5

        when:
        discountService.delete(discountId)
        sessionFactory.currentSession.flush()

        then:
        discountService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        Discount discount = new Discount()
        discountService.save(discount)

        then:
        discount.id != null
    }
}
